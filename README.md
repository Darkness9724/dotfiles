Install:

```
git clone https://gitlab.com/Darkness9724/dotfiles
dotdrop --cfg=dotfiles/config.yaml install
```

Update:

```
git pull
dotdrop install
```

Credits:
[dotfiles/config/mpv/shaders/FSR.glsl](https://gist.github.com/agyild/82219c545228d70c5604f865ce0b0ce5/w)
[dotfiles/config/mpv/shaders/FSRCNNX.glsl](https://github.com/igv/FSRCNN-TensorFlow)
[dotfiles/config/mpv/shaders/KrigBilateral.glsl](https://gist.github.com/igv/a015fc885d5c22e6891820ad89555637)
[dotfiles/config/mpv/shaders/SSimDownscaler.glsl](https://gist.github.com/igv/36508af3ffc84410fe39761d6969be10)
