if ("paru" | is-success) {
    alias pasync = paru -Sy
    alias parupd = paru -Su
    alias parupg = paru -Syu 
}

alias update-grub = sudo grub-mkconfig -o /boot/grub/grub.cfg
alias mingw32-gcc = x86_64-w64-mingw32-gcc
alias less = bat -p
alias sd-webui = "cd /run/media/darkness9724/Storage/stable-diffusion-webui/; ./webui.sh"
alias dotdrop = /usr/bin/dotdrop $"--cfg=($env.HOME)/.local/src/dotfiles/config.yaml"
alias gamescope = /usr/bin/gamescope -f -U -W 2560 -H 1440 -r 75

def "git reset --hard" [commit] {
    git stash
    /usr/bin/git reset --hard $commit
}

def nohup [command] {
    /usr/bin/nohup $command | save -rf ([
            "/ram/",
            ($command | str replace "/" "" | str replace -sa "/" "_" ),
            ".nohup"]
        | str join)
}
