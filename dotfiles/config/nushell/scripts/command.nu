def is-success [] {
    each { |command|
        if not (which $command | get built-in | is-empty) {
            true
        } else {
            false
        }
    }
}
