# Nushell Environment Config File
#
# version = 0.78.0

def create_left_prompt [] {
    mut home = ""
    try {
        $home = $env.HOME
    }

    let dir = ([
        ($env.PWD | str substring 0..($home | str length) | str replace -s $home "~"),
        ($env.PWD | str substring ($home | str length)..)
    ] | str join)

    let path_segment = if (is-admin) {
        $"(ansi red_bold)($dir)"
    } else {
        $"(ansi green_bold)($dir)"
    }

    $path_segment
}

def create_right_prompt [] {
    let time_segment = ([
        (date now | date format '%m/%d/%Y %r')
    ] | str join)

    let exit_code = ([
        "<",
        $env.LAST_EXIT_CODE,
        "> "
    ] | str join)

    [$exit_code, $time_segment] | str join
}

# Use nushell functions to define your right and left prompt
let-env PROMPT_COMMAND = {|| create_left_prompt }
let-env PROMPT_COMMAND_RIGHT = {|| create_right_prompt }

# The prompt indicators are environmental variables that represent
# the state of the prompt
let-env PROMPT_INDICATOR = {|| "> " }
let-env PROMPT_INDICATOR_VI_INSERT = {|| ": " }
let-env PROMPT_INDICATOR_VI_NORMAL = {|| "> " }
let-env PROMPT_MULTILINE_INDICATOR = {|| "::: " }

# Specifies how environment variables are:
# - converted from a string to a value on Nushell startup (from_string)
# - converted from a value back to a string when running external commands (to_string)
# Note: The conversions happen *after* config.nu is loaded
let-env ENV_CONVERSIONS = {
  "PATH": {
    from_string: { |s| $s | split row (char esep) | path expand -n }
    to_string: { |v| $v | path expand -n | str join (char esep) }
  }
  "Path": {
    from_string: { |s| $s | split row (char esep) | path expand -n }
    to_string: { |v| $v | path expand -n | str join (char esep) }
  }
}

# Directories to search for scripts when calling source or use
#
# By default, <nushell-config-dir>/scripts is added
let-env NU_LIB_DIRS = [
    ($nu.config-path | path dirname | path join 'scripts')
]

# Directories to search for plugin binaries when calling register
#
# By default, <nushell-config-dir>/plugins is added
let-env NU_PLUGIN_DIRS = [
    ($nu.config-path | path dirname | path join 'plugins')
]

# To add entries to PATH (on Windows you might use Path), you can use the following pattern:
# let-env PATH = ($env.PATH | split row (char esep) | prepend '/some/path')

let-env PATH = ($env.PATH | split row (char esep) | prepend '~/.local/bin' | prepend '~/.local/share/cargo/bin' | prepend '/opt/rocm/bin')

# XDG
let-env XDG_CACHE_HOME = ($env.HOME | path join '.cache')
let-env XDG_CONFIG_HOME = ($env.HOME | path join '.config')
let-env XDG_DATA_HOME = ($env.HOME | path join '.local/share')
let-env XDG_STATE_HOME = ($env.HOME | path join '.local/state')

# KDE
let-env KDEHOME = ($env.XDG_CONFIG_HOME | path join 'kde')
let-env PLASMA_USE_QT_SCALING = 1
let-env QT_AUTO_SCREEN_SCALE_FACTOR = 1

# Mozilla
let-env MOZ_ACCELERATED = 1
let-env MOZ_DISABLE_RDD_SANDBOX = 1
let-env MOZ_ENABLE_WAYLAND = 1
let-env MOZ_WEBRENDER = 1

# Ruby
let-env BUNDLE_USER_CACHE = ($env.XDG_CACHE_HOME | path join 'bundle')
let-env BUNDLE_USER_CONFIG = ($env.XDG_CONFIG_HOME | path join 'bundle')
let-env BUNDLE_USER_PLUGIN = ($env.XDG_DATA_HOME | path join 'bundle')
let-env GEM_HOME = ($env.XDG_DATA_HOME | path join 'gem')
let-env GEM_SPEC_CACHE = ($env.XDG_CACHE_HOME | path join 'gem')

# Wine
let-env WINEPREFIX = ($env.XDG_DATA_HOME | path join 'wine/default')
let-env WINEDEBUG = 'fixme-all'
let-env WINEESYNC = 1
let-env WINEFSYNC = 1
let-env DXVK_FRAME_RATE = 75
let-env VKD3D_CONFIG = "dxr11,dxr"

# curl
let-env ftp_proxy = http://127.0.0.1:8118
let-env http_proxy = http://127.0.0.1:8118
let-env https_proxy = http://127.0.0.1:8118
let-env rsync_proxy = http://127.0.0.1:8118

# Octave
let-env OCTAVE_HISTFILE = ($env.XDG_CACHE_HOME | path join 'octave-history')
let-env OCTAVE_SITE_INITFILE = ($env.XDG_CONFIG_HOME | path join 'octave/octaverc')

# Rust
let-env CARGO_HOME = ($env.XDG_DATA_HOME | path join 'cargo')
let-env RUSTUP_HOME = ($env.XDG_DATA_HOME | path join 'rustup')

# TeX
let-env TEXMFCONFIG = ($env.XDG_CONFIG_HOME | path join 'texmf')
let-env TEXMFHOME = ($env.XDG_DATA_HOME | path join 'texmf')
let-env TEXMFVAR = ($env.XDG_CACHE_HOME | path join 'texmf')

# Misc
let-env _JAVA_OPTIONS = '-Dawt.useSystemAAFontSettings=on'
let-env CALCHISTFILE = ($env.XDG_CACHE_HOME | path join 'calc_history')
let-env GTK2_RC_FILES = ($env.XDG_CONFIG_HOME | path join 'gtk-2.0/gtkrc')
let-env IPFS_PATH = ($env.XDG_DATA_HOME | path join 'ipfs')
let-env MAXIMA_USERDIR = ($env.XDG_CONFIG_HOME | path join 'maxima')
let-env NPM_CONFIG_USERCONFIG = ($env.XDG_CONFIG_HOME | path join 'npm/npmrc')
let-env PARALLEL_HOME = ($env.XDG_DATA_HOME | path join 'parallel')
let-env WGETRC = ($env.XDG_CONFIG_HOME | path join 'wget/wgetrc')
let-env WORKON_HOME = ($env.XDG_DATA_HOME | path join 'virtualenvs')
let-env GNUPGHOME = ($env.XDG_DATA_HOME | path join 'gnupg')
let-env ELECTRON_TRASH = "kioclient"
let-env DIFFPROG = "meld"
let-env PAGER = "bat -p"
let-env MAGICK_OCL_DEVICE = true
let-env RADV_PERFTEST = "nggc,rt"

# Templorary disabled due https://github.com/libsdl-org/SDL/issues/6438 
#let-env SDL_DYNAMIC_API = "/usr/lib/libSDL2.so"
#let-env SDL_VIDEODRIVER = "wayland"
