if $env.XDG_SESSION_TYPE? == "tty" and $env.XDG_VTNR? == "1" {
    let-env XDG_SESSION_TYPE = wayland
    dbus-run-session startplasma-wayland
}
